# -*- coding: utf-8 -*-
"""
Created on Mar 18 2022

@author: akotlik
"""

import scores
import tiers
from datetime import date
from datetime import timedelta
import sys
import logging
import logging.handlers
import optparse
import os
import socket

import sqlalchemy
import pandas as pd


sql_scripts_path = './SQL_scripts/'   

def get_log_file_name(folder, db_name=None):
    log_folder = ""
    if None == folder or "" == folder:
        abspath = os.path.abspath(__file__)
        dname = os.path.dirname(abspath)

        log_folder = dname + "/../log/"
        if not os.path.isdir(log_folder):
            log_folder = dname + "/"
    else:
        log_folder = folder + "/"
    if db_name is None:
        db_name = 'UNKNOWN'

    return log_folder + (
        "customer_tiers_{:s}_{:s}.log".format(socket.gethostname(), db_name))


def main(argv):
   
    cur_date = date.today()
    start_date = date.fromisoformat('2022-01-01')
            
    parser = optparse.OptionParser()
    parser.add_option("-d", "--dbname", dest="database_name", type="string", action="store", help="Database name ")
    parser.add_option("-o", "--host", dest="host_name", type="string", action="store",
                      help="Host name [default: %default]", default="localhost")
    parser.add_option("-p", "--port", dest="port", type="int", action="store", help="port [default: %default]",
                      default="5432")
    parser.add_option("-u", "--username", dest="username", type="string", action="store", help="Database user name")
    parser.add_option("-w", "--password", dest="password", type="string", action="store", help="Database password")
    parser.add_option("-l", "--logging_level", dest="logging_level", type="choice", action="store",
                      help="Logging level", choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"],
                      default="INFO")
    parser.add_option("-f", "--log_folder", dest="log_folder", type="string", action="store",
                      help="Logging directory, default ../log/")
    parser.add_option("-c", "--cur_date", dest="cur_date", type="string", action="store", help="Last date on which the tiers shall be defined - must be in the 'yyyy-mm-dd' ISO format")
    parser.add_option("-s", "--start_date", dest="start_date", type="string", action="store", help="First date of the range on which the tiers shall be defined - must be in the 'yyyy-mm-dd' ISO format")

    parser.add_option("-b", "--bally", dest="is_Bally", action="store_true",
                      help="Boolean flag - do we use Bally's data or Elite's.", default=True)
    parser.add_option("-t", "--output", dest="f_Output", action="store_true",
                      help="Boolean flag - do we need to store tiers and KPIs to csv files", default=False)
 
    (options, args) = parser.parse_args(argv)

    if options.database_name is None:
        parser.error("Database name is required")
    if options.username is None:
        parser.error("Database username name is required")

    conn_string = "host=\'" + options.host_name + "\' dbname=\'" + options.database_name + "\' user=\'" + \
                  options.username + "\' port=\'" + str(options.port) + "\'"
    if options.password is not None:
        conn_string = conn_string + " password=\'" + options.password + "\'"
 

    log_file_name = get_log_file_name(options.log_folder, options.database_name)

    log_handler = logging.handlers.TimedRotatingFileHandler(log_file_name, when="midnight")
    log_formatter = logging.Formatter('%(asctime)s %(module)s %(levelname)s : %(message)s')
    log_handler.setFormatter(log_formatter)
    logger = logging.getLogger('customer_tiers')
    logger.addHandler(log_handler)
    logger.setLevel(options.logging_level)

    if options.cur_date is not None:
        try:
            cur_date = date.fromisoformat(options.cur_date ) # must be the date in the ISO format 'yyyy-mm-dd'
        except:
            logger.error('Passed wrong argument / wrong date format - cur_date must be "yyyy-mm-dd"')
            sys.exit('Passed wrong argument / wrong date format - must be "yyyy-mm-dd"')
      
    if options.start_date is not None:
        try:
            start_date = date.fromisoformat(options.start_date ) # must be the date in the ISO format 'yyyy-mm-dd'
        except:
            logger.error('Passed wrong argument / wrong date format - start_date must be "yyyy-mm-dd"')
            sys.exit('Passed wrong argument / wrong date format - start_date must be "yyyy-mm-dd"')

    tiers_query_file = 'History_Tiers.sql'

    try:
        engine_prod = sqlalchemy.create_engine('postgresql://%s:%s@%s:5432/%s'
                                          %(options.username, options.password, 
                                            options.host_name, options.database_name), echo=False)           
        with open(sql_scripts_path + tiers_query_file, 'r') as reader:
            query = reader.read()
            _ = pd.read_sql_query(query, con=engine_prod, params = {'end_date' : cur_date}) 
           
        logger.info('Created a connection to the production database %s with user %s at host %s'%(options.database_name, options.username, options.host_name))
    except:
        logger.error('Failed to create a connection to the production SQL database')       
    
    logger.info(f'The model will run from : {start_date.isoformat()}')
    logger.info(f'The model will run up to : {cur_date.isoformat()}')


    td = start_date
    if td <= cur_date:       
    # finding KPIs and scores for all customers for the passed date - cur_date
        df_scores = scores.get_scores(engine_prod, td.isoformat(), options.is_Bally, options.f_Output)   
        logger.info(df_scores.shape)
    # assigning tiers for all customers who are due on the passed date - cur_date
        df_tiers = tiers.get_tiers(engine_prod, df_scores, td.isoformat(), options.is_Bally, options.f_Output)
        logger.info(df_tiers.shape)
    
    td = td + timedelta(days=1)
    while td <= cur_date:    
        logger.info(' ** ')
        logger.info('new date')
        logger.info('****')
        td = td + timedelta(days=1)
    # finding KPIs and scores for all customers for the passed date - cur_date
        df_scores = scores.get_scores(engine_prod, td.isoformat(), options.is_Bally, options.f_Output)   
        logger.info(df_scores.shape)
    # assigning tiers for all customers who are due on the passed date - cur_date
        df_tiers = tiers.get_tiers(engine_prod, df_scores, td.isoformat(), options.is_Bally, options.f_Output)
        logger.info(df_tiers.shape)

    engine_prod.dispose()
 
    logger.info('--- The End ---')
    logger.info('-' * 80)

if __name__ == '__main__':
    main(sys.argv[1:])
    
