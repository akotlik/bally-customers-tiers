# -*- coding: utf-8 -*-
"""
Created on Mar 10 2022

@author: akotlik
"""
import numpy as np
import pandas as pd
import os
import sys
import random
from datetime import date
import sqlalchemy
from sklearn.preprocessing import PowerTransformer

from utilities_tiers import find_nearest, get_centroids, inertia, tiers_comparison
import scores

import logging
import logging.handlers
import optparse
import socket

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.options.display.float_format = '{:.2f}'.format
np.set_printoptions(precision=2)


SEED = 1970
random.seed(SEED)


sql_scripts_path = './SQL_scripts/'   
data_path = './data/'

num_iterations = 10
Tolerance = 0.5 # in percents %   

KPI_scores_columns = [
    'total_dep_relative', 'total_net_deposit_per_bet', 'bet_days_counts', 'net_deposits', 'bet_amounts', 'total_net_deposit', 'GGR', 'GGR_margin', 'total_GGR', 'total_GGR_margin']
cols_for_analysis = [
                      'GGR', 'GGR_margin', 'total_GGR', 'total_GGR_margin',
                      'net_deposits', 'total_net_deposit', 'total_dep_relative', 
                      'bet_days_counts',  'bets_all_counts', 'win_ratio',
                      'num_active_weeks', 'days_since_last_trans', 'num_months',
                      'num_days',
                      'all_bets_count', 'total_bet_amt', 'total_pay_amt',
                      'bet_amounts', 'payout_amount_w_fb',
                      'total_wthdrl_amt', 'total_dep_amount', 'total_net_deposit_per_bet',
                      'deposit_amounts', 'withdrawal_amounts',                                            
                      ]
KPIs_for_analysis = [
                      'GGR', 'GGR_margin', 'total_GGR', 'total_GGR_margin',
                      'net_deposits', 'total_net_deposit', 'total_dep_relative', 
                      'bet_days_counts',  'bets_all_counts',
                      'deposit_amounts', 'withdrawal_amounts',   
                      ]   


# coords of the centers of a stable cluster distribution hardcoded for those 
# cases when some clusters might become empty - especially for VIPs. 
# calculated on Ballys data

# ver 12
backup_centers = np.array(
    [[  0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [  0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [  0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [  0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [    0.8,          150,       6,       2000,       5000,      8000,        2000,    0.6,      15000,      0.6],
    [    0.9,          200,       9,      10000,      20000,    100000,       10000,    0.8,      40000,      0.8]])

# original non-normalized coords of the clusters initiated by hybrid model based 
# on the combined data from both Elite and Bally, and later adjusted based 
# on empirical observations/experiments
# ver 12
centers_6 = np.array(
    [[ -100.0,        -200,       5,      -3000,       5000,    -10000,       -2000,  -1.65,     -15000,     -2.0],
    [  -20.0,          -20,       3,       -500,        700,     -1000,        -500,   -0.5,      -1000,     -0.5],
    [   0.3,          20,       1,         80,        400,       200,         100,    0.3,       500,      0.3],
    [   0.6,          70,       3,        500,       1000,      1000,        800,    0.5,       5000,      0.5],
    [  0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [  0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ])
  
def reclustering(df_iterate, cluster_assigned, new_cluster, KPI_columns, N_clusters = 6, f_Print = False):
    '''
    Function determining change in inertia for the next iteration of reclustering,
    and defining if the reclustering shall continue

    Parameters
    ----------
    df_iterate : pandas dataframe
        contains all customers 
    cluster_assigned : string
        name of the feature/column for the tiers assigned before
    new_cluster : string
        name of the feature/column for the newly assigned tiers
    KPI_columns : list
        columns used to compute inertia of the clusters
    N_clusters : int
        Number of clusters. The default is 6.
    f_Print : boolean, optional
        flag - do we want to print out debugging info. The default is False.

    Returns
    -------
    bool
        True if the change in inertia became smaller than the given threshold.

    '''
    logger = logging.getLogger('customer_tiers')

    old_inertia = np.zeros(N_clusters)
    new_inertia = np.zeros(N_clusters)
    for i in range(N_clusters):
        old_num_per_cluster = df_iterate[cluster_assigned].value_counts()
        new_num_per_cluster = df_iterate[new_cluster].value_counts()
        #  there is a chance that we will have some clusters empty
        try:
            old_num_per_cluster = old_num_per_cluster[i]
        except:
            old_num_per_cluster = 0
        try:
            new_num_per_cluster = new_num_per_cluster[i]
        except:
            new_num_per_cluster = 0

        old_clust = df_iterate.loc[df_iterate[cluster_assigned] == i][KPI_columns].values
        new_clust = df_iterate.loc[df_iterate[new_cluster] == i][KPI_columns].values
        if old_clust.shape[0] > 0:
            old_inertia[i] = inertia(old_clust)
        else:
            old_inertia[i] = 0.000001
        if new_clust.shape[0] > 0:
            new_inertia[i] = inertia(new_clust)
        else:
            new_inertia[i] = 0.000001
        if new_inertia[i] == 0:
            new_inertia[i] = 0.000001
        logger.info('Cluster %i :'%(i))
        logger.info(' - Ratio of previous inertia to the new inertia = %.2f'%(old_inertia[i] /new_inertia[i]))
        logger.info(' - Previous Inertia = %.2f  -->  New Inertia = %.2f'%(old_inertia[i], new_inertia[i]))
        logger.info(' - # of customers in the cluster before = %i  -->  # of customers reassigned = %i'%(old_num_per_cluster, new_num_per_cluster))


    logger.info('Total Inertia before - %.2f, after - %.2f'%(old_inertia.sum(), new_inertia.sum()))    
    logger.info('Change in Total Inertia %.2f%%'%(np.abs(old_inertia.sum() - new_inertia.sum())/new_inertia.sum()* 100))
    logger.info('-'*60)
    
    if np.abs(old_inertia.sum() - new_inertia.sum())/new_inertia.sum() * 100 > Tolerance :
      return False
    else:
      return True  
 

def confirm_VIPs(old_tiers, cur_tiers):
    '''
    Function responsible for control over the tiers assignment for 
    edge case customers : Sharps and VIPs

    Must check if VIPs tiers assigned correctly:
        - should not be dropped/downgraded before 90 days threshold reached
        - Tiers 0/1 should not be upgraded to VIPs - assign Tier 2 instead

    Parameters
    ----------
    old_tiers : pandas dataframe
        all tiers loaded from the previous day data
    cur_tiers : pandas dataframe
        a set of records only for the due customers

    Returns
    -------
    Pandas dataframe containing the computed tiers for all customers including VIPs.

    '''
    conf_tiers = cur_tiers.copy()
    for i in cur_tiers.index.values:
        # cheking if the customer had been awarded any tiers before.
        # could be multiple assignments, 1 or none at all
        assigned = conf_tiers.loc[i, 'tier']
        # get method returns the assigned tier if the id is found or 1000 otherwise
        cond = old_tiers.tier.get(i, 1000) != 1000
        if (type(cond) == bool) | (type(cond) == np.bool_):
            # 1 assignment or none at all
            # if none had been assigned then we don't have to do anything,
            # just pass back the newly assigned tier
            if cond:
                old_due = old_tiers.loc[i]
                # it's a series
                old_tier = old_due['tier']
                # if it was a VIP tier and assigned just once then we need to keep it
                if old_tier >= 5:
                # if old_tier >= 4: # <- until we decide we have Tier 4 as VIP and Tier 5 as SuperVIP
                    conf_tiers.loc[i, 'tier'] = old_tier
                # prevent iffy jump from negative tiers straight to VIPs
                elif (old_tier < 2) & (assigned > 3):
                    conf_tiers.loc[i, 'tier'] = 2
                        
        else:
            # it's a dataframe, multiple tier assignments, we need to choose the last one
            old_due = old_tiers.loc[i]
            old_due  = old_due.sort_values(by = ['assigned_date'], ascending = False)
            old_tier = old_due.iloc[0, 0]
            # only for VIPs, select last 3 tiers assigned to check if 
            # the 90 days threshold is reached
            if old_tier >= 5:           
            # if old_tier >= 4: # <- until we decide we have Tier 4 as VIP and Tier 5 as SuperVIP
                last3 = old_due.iloc[:3]
                old_vals = last3.tier.value_counts()
                old_tier_counts = old_vals.loc[old_tier]
                if (old_tier_counts == 3):
                    conf_tiers.loc[i, 'tier'] = assigned
                else:
                    conf_tiers.loc[i, 'tier'] = old_tier       
            # prevent iffy jump from negative tiers straight to VIPs
            elif (old_tier < 2) & (assigned > 3):
                conf_tiers.loc[i, 'tier'] = 2
    return conf_tiers


# set tiers when no previous tiers were ever assigned
def get_first_tiers(engine, df_KPIs, cur_date, f_Output = False, is_Bally = True):
    '''
    Function for setting tiers, when there is no previous known state. 
    Assigns tiers to all customers based on precomuted clusters coordinates. 
    Should be called only once to start database population, beginning on 
    the cur_date.

    Parameters
    ----------
    engine : SQLAlchemy object, required
    df_KPIs : Pandas dataframe, required
        KPIs ans scores precomuted on the given date.       
    cur_date : string representing date in yyyy-mm-dd format, required
        given date  for computing current KPI and tiers.
    f_Output : boolean, optional
        flag - do we need to store tiers and KPIs to csv files
        The default is False.
    is_Bally : boolean, optional
        flag - do we use Bally's data or Elite's. The default is True for Bally.

    Returns
    -------
    Pandas dataframe containing the computed tiers along with KPIs for analysis.
    '''
    logger = logging.getLogger('customer_tiers')

    # del_query_file = 'Delete_Tiers.sql'
    global centers_6
    
    logger.info('Total number of customers to be assigned the Tiers: %i'%df_KPIs.shape[0])
    
    base_columns = df_KPIs.columns.tolist()
    base_columns = base_columns[:28] + base_columns[-4:]
    df_KPI_and_scores = df_KPIs.copy()
    
    logger.info('Number of clients with no deposits and no withdrawals - %i out of total %i'%(df_KPI_and_scores.loc[df_KPI_and_scores.total_dep_amount == 0.1].shape[0], df_KPI_and_scores.shape[0]))
    logger.info('Number of clients who did not make deposits, but had withdrawals - %i out of total %i'%(df_KPI_and_scores.loc[df_KPI_and_scores.total_dep_amount == 1].shape[0], df_KPI_and_scores.shape[0]))
    
    # We will drop those who didn't do any deposits and withdrawals, just used free bets, and who hasn't been active for more 90 days (consider them churned by now)
    df_free_betters = df_KPI_and_scores.copy().loc[(df_KPI_and_scores.days_since_last_trans > 90) & (df_KPI_and_scores.total_dep_amount == 0.1)]

    logger.info('Number of free betters to be dropped: %i'%df_free_betters.shape[0])
    
    df_KPI_and_scores.drop(df_KPI_and_scores.loc[(df_KPI_and_scores.days_since_last_trans > 90) & (df_KPI_and_scores.total_dep_amount == 0.1)].index, inplace = True)

    logger.info('Number of remaining customers, to be assigned the Tiers: %i'%df_KPI_and_scores.shape[0])
    
    
    # Initial clusters assignments
    # Initialization from pre-computed centers
    # Load the centers of 6 clusters

    logger.info(centers_6)
      
    # Normalize the scores for clustering algorithm
    Xs = df_KPI_and_scores[KPI_scores_columns].values
    P_Tr = PowerTransformer()
    Xt = P_Tr.fit_transform(Xs)

    logger.info(Xt.shape)
        
    df_iterate = df_KPI_and_scores.copy()
    df_iterate[KPI_scores_columns] = Xt
    
    # new change - normalize centroids, because now they are in a raw values scale
    centers_6 = P_Tr.transform(centers_6)
    
    # normalize coords of the preserved centers
    backup_centers_norm = P_Tr.transform(backup_centers)
    
    # Clustering by KMeans
    # check if the VIPs have lifespan more than 90 days, downgrade to Tier 3 those how are newcomers
    df_iterate['new_cluster_6'] = df_iterate.apply(lambda x: find_nearest(x, KPI_scores_columns, centers_6), axis = 1)

    logger.info('After first reassigning')
    logger.info(df_iterate['new_cluster_6'].value_counts())
    
    logger.info('Tiers reassigning')
    for i in range(num_iterations):

        logger.info('Iteration #%i :'%i)
        
        df_iterate['6_tiers'] = df_iterate['new_cluster_6']
        centers_6 = get_centroids(df_iterate, '6_tiers', KPI_scores_columns)
        centers_6[4] = backup_centers_norm[4]
        centers_6[5] = backup_centers_norm[5]
        df_iterate['new_cluster_6'] = df_iterate.apply(lambda x: find_nearest(x, KPI_scores_columns, centers_6), axis = 1)
        if reclustering(df_iterate, '6_tiers', 'new_cluster_6', KPI_scores_columns, 6):

            logger.info('Stopped because the change in Inertia is smaller than tolerance threshhold of %.2f%%'%Tolerance)
            break
        
    # Saving newly assigned tiers into csv
    df_KPI_and_scores['tiers'] = df_iterate['new_cluster_6']

    logger.info('Final distribution')
    logger.info(df_iterate['new_cluster_6'].value_counts())
    for i in range(6):
        # logger.info('Tier %i : %.2f'%(i,(df_iterate.loc[df_iterate.new_cluster_6==i].shape[0] / df_iterate.shape[0])*100), '%')
        logger.info(f'Tier {i} : {(df_iterate.loc[df_iterate.new_cluster_6==i].shape[0] / df_iterate.shape[0])*100 : .2f}%')
    
    logger.info('Clusters analysis:')
 
    for KPI in KPIs_for_analysis:
      tiers_comparison(df_KPI_and_scores, KPI, 'tiers', 6)
    
    # Lets include back the cusomers who only used initial promo offers and went dormant after that.  
    df_free_betters['tiers'] = -1
    df_KPI_and_scores_full = pd.concat([df_KPI_and_scores, df_free_betters])
    logger.info('Total number of all customers including free-betters: %i'%df_KPI_and_scores_full.shape[0])
    logger.info('-----------')

    df_KPI_and_scores_full = df_KPI_and_scores_full[[ 'tiers']+cols_for_analysis]
    if f_Output:
        if is_Bally:
            df_KPI_and_scores_full.to_csv(data_path +'Bally_Tiers_%s.csv'%cur_date, index=True)
        else:
            df_KPI_and_scores_full.to_csv(data_path +'Elite_Tiers_%s.csv'%cur_date, index=True)
       
    assg_tiers = pd.DataFrame(columns = ['customer_id', 'tier', 'assigned_date'])
    assg_tiers['customer_id'] = df_KPI_and_scores_full.index.values
    assg_tiers.set_index('customer_id', inplace = True)
    assg_tiers['tier'] = df_KPI_and_scores_full['tiers']
    assg_tiers['assigned_date'] = cur_date   

    # update records of tiers assigned for the customers that are due on the current date
    # should be SQL script here inserting only new records - not all at once
    if f_Output:
        assg_tiers.to_csv(data_path + 'assigned_tiers.csv', index = True)

    try:
        # engine = sqlalchemy.create_engine('postgresql://%s:%s@%s:5432/dwh'%(db_user, db_password, db_host))  
        # we might want to make sure the table is empty - delete all records, but don't have the permission
        assg_tiers.to_sql('ds_customer_tiers', con = engine, schema='dat', if_exists='append', index=True, index_label=None, chunksize=None, dtype=None, method=None)  
        # engine.dispose()                     
    except:
        logger.error('Failed saving Tiers data to the SQL database')
        
    return df_KPI_and_scores_full


def get_tiers_SQL(engine, cur_date, is_Bally = True):
# def get_tiers_SQL(db_user, db_password, db_host, cur_date, f_Print = False, is_Bally = True):
    '''
    Parameters
    ----------
    engine : SQLAlchemy object, required
    cur_date : string representing date in yyyy-mm-dd format, required
         given date  for computing current KPI and tiers.
    is_Bally : boolean, optional
        flag - do we use Bally's data or Elite's. The default is True for Bally.

    Returns
    -------
    Pandas dataframe containing the last computed tiers for all customers.
    '''
    logger = logging.getLogger('customer_tiers')

    # we will try to get the last known tiers from SQL database
    # on the given date
    tiers_query_file = 'Last_Tiers.sql'
    try:
        # engine = sqlalchemy.create_engine('postgresql://%s:%s@%s:5432/dwh'%(db_user, db_password, db_host))  
                      
        with open(sql_scripts_path + tiers_query_file, 'r') as reader:
            query = reader.read()
            # we want to find last known tiers by a certain date
            # df_tiers = pd.read_sql_query(query, con=engine, params = (cur_date,)) 
            df_tiers = pd.read_sql_query(query, con=engine, params = {'end_date' : cur_date}) 
        # engine.dispose()  
    except:
        logger.error('Failed getting tiers data from the SQL database')
        
    return df_tiers


def get_hist_tiers_SQL(engine, cur_date, is_Bally = True):
    '''
    Function to retrieve the history of all previously assigned tiers
    
    Parameters
    ----------
    engine : SQLAlchemy object, required
    cur_date : string representing date in yyyy-mm-dd format, required
         given date  for computing current KPI and tiers.
    is_Bally : boolean, optional
        flag - do we use Bally's data or Elite's. The default is True for Bally.

    Returns
    -------
    Pandas dataframe containing the last computed tiers for all customers.
    '''
    logger = logging.getLogger('customer_tiers')

    # we will try to get the last known tiers from SQL database
    # on the given date
    tiers_query_file = 'History_Tiers.sql'
    try:                      
        with open(sql_scripts_path + tiers_query_file, 'r') as reader:
            query = reader.read()
            # we want to find last known tiers by a certain date
            # df_tiers = pd.read_sql_query(query, con=engine, params = (cur_date,)) 
            df_tiers = pd.read_sql_query(query, con=engine, params = {'end_date' : cur_date}) 
    except:
        logger.error('Failed getting historical tiers data from the SQL database')
        
    return df_tiers


def get_tiers_frozen(engine, df_KPIs, cur_date, precomputed_tier = 'SQL', f_Output = False, is_Bally = True):
    '''
    Main function for setting tiers, given previous known state. 
    Assigns tiers only to the customers who are due on the given date, 
    the rest are frozen forming the stable cores of all clusters.

    Parameters
    ----------
    engine : SQLAlchemy object, required
    df_KPIs : Pandas dataframe, required
        KPIs ans scores precomuted on the given date.       
    cur_date : string representing date in yyyy-mm-dd format, required
        given date  for computing current KPI and tiers.
    precomputed_tier : string
        filename of a csv file with previous computed tiers, if not using SQL. 
        The default is 'SQL'.
    f_Output : boolean, optional
        flag - do we need to store tiers and KPIs to csv files
        The default is False.
    is_Bally : boolean, optional
        flag - do we use Bally's data or Elite's. The default is True for Bally.

    Returns
    -------
    Pandas dataframe containing the computed tiers along with KPIs for analysis.

    '''    
    logger = logging.getLogger('customer_tiers')

    global backup_centers
    
    if precomputed_tier == 'SQL':
        pre_tiers = get_tiers_SQL(engine, cur_date, is_Bally)
        # for now lets rename for backward compatibility - and later rename tiers to tier
        pre_tiers.rename(columns = {'tier' : 'tiers'}, inplace = True)
        pre_tiers.set_index('customer_id', inplace = True)
        if pre_tiers.iloc[0].assigned_date.isoformat() == cur_date:
            logger.info('Tiers have beed already assigned for this date : %s'%str(cur_date))
            return
    else:
        try:
            pre_tiers = pd.read_csv(data_path + precomputed_tier)
            pre_tiers.set_index('customer_id', inplace = True)
        except:
            print(sys.exc_info()[1])
            return
        
    logger.info('Total number of customers to be assigned the Tiers: %i'%df_KPIs.shape[0])
    
    base_columns = df_KPIs.columns.tolist()
    base_columns = base_columns[:28] + base_columns[-4:]
    df_KPI_and_scores = df_KPIs.copy()
    
    # We will drop those who didn't do any deposits and withdrawals, just used 
    # free bets, and who hasn't been active for more 90 days 
    # (consider them churned by now)
    df_free_betters = df_KPI_and_scores.copy().loc[(df_KPI_and_scores.days_since_last_trans > 90) & (df_KPI_and_scores.total_dep_amount == 0.1)]

    logger.info('Number of clients with no deposits and no withdrawals - %i out of total %i'%(df_KPI_and_scores.loc[df_KPI_and_scores.total_dep_amount == 0.1].shape[0], df_KPI_and_scores.shape[0]))
    logger.info('Number of clients who did not make deposits, but had withdrawals - %i out of total %i'%(df_KPI_and_scores.loc[df_KPI_and_scores.total_dep_amount == 1].shape[0], df_KPI_and_scores.shape[0]))
    logger.info('Number of free betters to be dropped: %i'%df_free_betters.shape[0])
    
    df_KPI_and_scores.drop(df_KPI_and_scores.loc[(df_KPI_and_scores.days_since_last_trans > 90) & (df_KPI_and_scores.total_dep_amount == 0.1)].index, inplace = True)

    logger.info('Number of remaining customers, to be assigned the Tiers: %i'%df_KPI_and_scores.shape[0])
        
    # Initial clusters assignments - load last known tiers   
    df_clusters = df_KPI_and_scores.merge(pre_tiers[['tiers']], on = 'customer_id', how = 'left')
    df_clusters_due = df_clusters.loc[df_clusters.due == 0]
    df_stable = df_clusters.copy().loc[df_clusters.due != 0]
    df_new = df_stable[df_stable['tiers'].isna()]
    df_free_bets = df_stable[df_stable['tiers'] == -1]

    logger.info('Number of customers who are due to be assigned new Tiers on this date: %i'%df_clusters_due.shape[0])
    logger.info('Number of customers who are NOT due to be assigned a new Tier, but lack tiers on this date: %i'%df_stable['tiers'].isna().sum())
    logger.info('Number of customers who have been marked as free betters before this date: %i'%df_free_bets.shape[0])
    # sanity check - do we have any missing values for KPIs ?
    logger.info('-' * 80)
    logger.info('Missing KPIs values? :')
    logger.info(df_stable[KPI_scores_columns].isna().sum())
    logger.info('-' * 80)

    # number of customers who have not been assigned any tiers before - we 
    # shall assign it now even though they are not due yet
    df_clusters_due = pd.concat([df_clusters_due, df_new])   
    # number of customers who got marked as a free better before - we 
    # shall assign it now even if they are not due yet
    df_clusters_due = pd.concat([df_clusters_due, df_free_bets])   
    df_stable.dropna(inplace = True)
    df_stable.drop(df_free_bets.index, inplace = True)

    # Normalize the scores for clustering algorithm
    P_Tr = PowerTransformer()
    Xs = df_stable[KPI_scores_columns].values
    P_Tr.fit(Xs)
    Xt = P_Tr.transform(Xs)

    logger.info(Xt.shape)
    
    df_stable_norm = df_stable.copy()
    df_stable_norm[KPI_scores_columns] = Xt

    # normalize coords of the preserved centers
    backup_centers_norm = P_Tr.transform(backup_centers)

    # Compute the centers of the 6 clusters based on 'frozen' customers only
    frozen_centers_6 = get_centroids(df_stable_norm, 'tiers', KPI_scores_columns, N_clusters = 6)

    logger.info('Current centorids:')
    logger.info(pd.DataFrame(P_Tr.inverse_transform(frozen_centers_6), columns = KPI_scores_columns))
    
    frozen_centers_6[4] = backup_centers_norm[4]
    frozen_centers_6[5] = backup_centers_norm[5]
    # Failsafe - Replace center coords with preserved ones if a cluster becomes empty
    if np.isnan(frozen_centers_6).sum() > 0 :
        for cc in range(4):
            if np.isnan(frozen_centers_6[cc]).sum() > 0 :

                logger.warning('Empty Tier # %i'%cc)
                frozen_centers_6[cc] = backup_centers_norm[cc]

    logger.info('Current centorids with preserved VIP tiers:')
    logger.info(pd.DataFrame(P_Tr.inverse_transform(frozen_centers_6), columns = KPI_scores_columns))
        # print(pd.DataFrame(frozen_centers_6, columns = KPI_scores_columns))
    
    df_iterate = df_clusters_due.copy()
    Xs = df_clusters_due[KPI_scores_columns].values
    Xt = P_Tr.transform(Xs)

    logger.info(Xt.shape)
    df_iterate[KPI_scores_columns] = Xt
        
    # Clustering by KMeans
    df_iterate['new_cluster_6'] = df_iterate.apply(lambda x: find_nearest(x, KPI_scores_columns, frozen_centers_6), axis = 1)
    
    logger.info('After first reassigning')
    logger.info(df_iterate['new_cluster_6'].value_counts())
    logger.info('Tiers reassigning')

    for i in range(num_iterations):
        logger.info('Iteration #%i :'%i)
        
        df_iterate['tiers'] = df_iterate['new_cluster_6']
        # find centers of the full clusters
        df_full = pd.concat([df_stable_norm, df_iterate])
        centers_6 = get_centroids(df_full, 'tiers', KPI_scores_columns)
        # replace center coords with preserved ones if the cluster becomes empty
        if np.isnan(centers_6).sum() > 0:
            for cc in range(6):
                if np.isnan(centers_6[cc]).sum() > 0 :
                    logger.warning('Empty Tier # %i'%cc)
                    centers_6[cc] = backup_centers_norm[cc]
                    
        # apply re-clustering only to the customers who are due on this date
        df_iterate['new_cluster_6'] = df_iterate.apply(lambda x: find_nearest(x, KPI_scores_columns, centers_6), axis = 1)

        if reclustering(df_iterate, 'tiers', 'new_cluster_6', KPI_scores_columns, 6):
            logger.info('Stopped because the change in Inertia is smaller than tolerance threshhold of %.2f%%'%Tolerance)
            break

    # lets check if there are some VIPs that are being downgraded before 
    # 90 days threshold for maintaining the VIP status
    cur_iter = pd.DataFrame(columns = ['customer_id', 'tier', 'assigned_date'])
    cur_iter['customer_id'] = df_iterate.index.values
    cur_iter.set_index('customer_id', inplace = True)
    cur_iter['tier'] = df_iterate['new_cluster_6'].values
    cur_iter['assigned_date'] = cur_date

    # need to get the whole history of previously assigned ters
    # either retrieve from the SQL database or a csv file
    try:
        assg_tiers = get_hist_tiers_SQL(engine, cur_date, is_Bally)
    except:
        logger.warning('No tiers have been saved to SQL database, trying to retrieve from the local files.')
        assg_tiers = pd.DataFrame(columns = ['customer_id', 'tier', 'assigned_date'])
        try:
            assg_tiers = pd.read_csv(data_path + 'assigned_tiers.csv')
        except:
            logger.warning('No saved tiers found in the file: ' + data_path + 'assigned_tiers.csv')    
    
    assg_tiers.set_index('customer_id', inplace = True)

    confirmed_tiers = confirm_VIPs(assg_tiers, cur_iter)
    confirmed_tiers_comb = pd.concat([assg_tiers, confirmed_tiers])
    
    # update records of tiers assigned for due customers on the date
    # should be SQL script here inserting only new records - not all at once
    if f_Output:
        confirmed_tiers_comb.to_csv(data_path + 'assigned_tiers.csv', index = True)

    try:
        confirmed_tiers.to_sql('ds_customer_tiers', con = engine, schema='dat', if_exists='append', index=True, index_label=None, chunksize=None, dtype=None, method=None)  
    except:
        logger.error('Failed saving data to the SQL database')

    # merging 'frozen' set of customers with re-evaluated customers
    # restore KPI values to original instead of normalized ones
    df_iterate['tiers'] = confirmed_tiers['tier']
    df_iterate_org = df_iterate.copy()
    df_iterate_org[KPI_scores_columns] = df_clusters_due[KPI_scores_columns]
    # save the original non-noprmalized values of KPI since they are easier 
    # to evaluate
    df_full = pd.concat([df_stable, df_iterate_org])
        
    # Saving newly assigned tiers into csv
    logger.info('Final distribution')
    logger.info(df_full['tiers'].value_counts())
    for i in range(6):
        logger.info(f'Tier {i} : {(df_full.loc[df_full["tiers"]==i].shape[0] / df_full.shape[0])*100 : .2f}%')
        # logger.info('Tier %i : %.2f'%(i,(df_full.loc[df_full['tiers']==i].shape[0] / df_full.shape[0])*100), '"%"')
    
    logger.info('Clusters analysis:')
    for KPI in KPIs_for_analysis:
      tiers_comparison(df_full, KPI, 'tiers', 6)
    
    # Lets include back the cusomers who only used initial promo offers and 
    # went dormant after that.  
    df_free_betters['tiers'] = -1
    df_KPI_and_scores_full = pd.concat([df_full, df_free_betters])

    logger.info('Total number of all customers including free-betters: %i'%df_KPI_and_scores_full.shape[0])
    # logger.info('----------')

    df_KPI_and_scores_full = df_KPI_and_scores_full[[ 'tiers']+cols_for_analysis]
    if f_Output:
        if is_Bally:
            df_KPI_and_scores_full.to_csv(data_path +'Bally_Tiers_%s.csv'%cur_date, index=True)
        else:
            df_KPI_and_scores_full.to_csv(data_path +'Elite_Tiers_%s.csv'%cur_date, index=True)
        

    return df_KPI_and_scores_full


def tiers_first_time(engine, cur_date, is_Bally = True):
    '''
    Check if any tiers have been assigned before the given date.

    Parameters
    ----------
    engine : SQLAlchemy object, required
    cur_date : string representing date in yyyy-mm-dd format, required
        given date  for computing current KPI and tiers.
     is_Bally : boolean, optional
        flag - do we use Bally's data or Elite's. The default is True for Bally.

    Returns
    -------
    Possible values:
        - a list with 1 element ['SQL'] if the tiers were stored in the database;
        - a list of existing filenames with previously computed tiers;
        - an empty list if the tiers have not been computed at all.
    '''
    logger = logging.getLogger('customer_tiers')

    exst_tiers = []
    # first we will try to get data from SQL
    # alternatively we will try to get  tiers from stored csv files
    # if nothing succeeds we return empty list
    
    tiers_query_file = 'Last_Tiers.sql'
    if is_Bally:
        company = 'bally'
    else:
        company = 'elite'
        
    try:
                      
        with open(sql_scripts_path + tiers_query_file, 'r') as reader:
            query = reader.read()
            df_tiers = pd.read_sql_query(query, con=engine, params = {'end_date' : cur_date}) 
        if df_tiers.shape[0] > 0:
            exst_tiers = ['SQL']
            logger.info('Tiers have been assigned previously, using SQL')        
        else:
            logger.info('Tiers have not been assigned previously, using SQL') 
    except:
        data_files = np.array(os.listdir(data_path))
        n_Files = len(data_files)
        if is_Bally:
            td_name = 'Bally_Tiers_' + cur_date
            t_name = 'Bally_Tiers'
            company = 'Bally'
        else:
            td_name = 'Elite_Tiers_' + cur_date
            t_name = 'Elite_Tiers'
            company = 'Elite'
        if n_Files > 0:
            sub_data_files = np.array([l for l in data_files if t_name in l])
            logger.info('Number of files with computed tiers for %s is %i'%(company, len(sub_data_files)))
            if len(sub_data_files) > 0:
                exst_tiers = np.sort(sub_data_files[sub_data_files < td_name])
                n_exst = len(exst_tiers)
                if n_exst > 0:
                    logger.info('Tiers have been assigned previously %i times'%n_exst)        
                else:
                    logger.info('There were NO Tiers assigned before %s'%cur_date)        
    return exst_tiers
    

def get_tiers(engine, df_KPIs, cur_date, is_Bally = True, f_Output = False):
    '''
    Main function that should be called if used as a module

    Parameters
    ----------
    engine : SQLAlchemy object, required
    df_KPIs : Pandas dataframe, required
        KPIs ans scores precomuted on the given date.       
    cur_date : string representing date in yyyy-mm-dd format, required
        given date  for computing current KPI and tiers.
    is_Bally : boolean, optional
        flag - do we use Bally's data or Elite's. The default is True for Bally.
    f_Output : boolean, optional
        flag - do we need to store tiers and KPIs to csv files
        The default is False.

    Returns
    -------
    Pandas dataframe containing the computed tiers along with KPIs for analysis.

    '''
    precomputed_tiers = tiers_first_time(engine, cur_date, is_Bally)
    if len(precomputed_tiers) > 0:
        new_tiers = get_tiers_frozen(engine, df_KPIs, cur_date, precomputed_tiers[-1], f_Output, is_Bally)
    else:
        new_tiers = get_first_tiers(engine, df_KPIs, cur_date, f_Output, is_Bally)
    return new_tiers 



def get_log_file_name(folder, db_name=None):
    log_folder = ""
    if None == folder or "" == folder:
        abspath = os.path.abspath(__file__)
        dname = os.path.dirname(abspath)

        log_folder = dname + "/../log/"
        if not os.path.isdir(log_folder):
            log_folder = dname + "/"
    else:
        log_folder = folder + "/"
    if db_name is None:
        db_name = 'UNKNOWN'

    return log_folder + (
        "customer_tiers_{:s}_{:s}.log".format(socket.gethostname(), db_name))


def main(argv):
   
    cur_date = date.today()
            
    parser = optparse.OptionParser()
    parser.add_option("-d", "--dbname", dest="database_name", type="string", action="store", help="Database name ")
    parser.add_option("-o", "--host", dest="host_name", type="string", action="store",
                      help="Host name [default: %default]", default="localhost")
    parser.add_option("-p", "--port", dest="port", type="int", action="store", help="port [default: %default]",
                      default="5432")
    parser.add_option("-u", "--username", dest="username", type="string", action="store", help="Database user name")
    parser.add_option("-w", "--password", dest="password", type="string", action="store", help="Database password")
    parser.add_option("-l", "--logging_level", dest="logging_level", type="choice", action="store",
                      help="Logging level", choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"],
                      default="INFO")
    parser.add_option("-f", "--log_folder", dest="log_folder", type="string", action="store",
                      help="Logging directory, default ../log/")
    parser.add_option("-c", "--date", dest="cur_date", type="string", action="store", help="Current date - must be in the 'yyyy-mm-dd' ISO format")

    parser.add_option("-b", "--bally", dest="is_Bally", action="store_true",
                      help="Boolean flag - do we use Bally's data or Elite's.", default=True)
    parser.add_option("-e", "--elite", dest="is_Bally", action="store_false",
                      help="Boolean flag - do we use Bally's data or Elite's.")
    parser.add_option("-t", "--output", dest="f_Output", action="store_true",
                      help="Boolean flag - do we need to store tiers and KPIs to csv files", default=False)
 
    (options, args) = parser.parse_args(argv)

    if options.database_name is None:
        parser.error("Database name is required")
    if options.username is None:
        parser.error("Database username name is required")

    conn_string = "host=\'" + options.host_name + "\' dbname=\'" + options.database_name + "\' user=\'" + \
                  options.username + "\' port=\'" + str(options.port) + "\'"
    if options.password is not None:
        conn_string = conn_string + " password=\'" + options.password + "\'"
 

    log_file_name = get_log_file_name(options.log_folder, options.database_name)

    log_handler = logging.handlers.TimedRotatingFileHandler(log_file_name, when="midnight")
    log_formatter = logging.Formatter('%(asctime)s %(module)s %(levelname)s : %(message)s')
    log_handler.setFormatter(log_formatter)
    logger = logging.getLogger('customer_tiers')
    logger.addHandler(log_handler)
    logger.setLevel(options.logging_level)

    tiers_query_file = 'History_Tiers.sql'

    if options.cur_date is not None:
        try:
            cur_date = date.fromisoformat(options.cur_date ) # must be the date in the ISO format 'yyyy-mm-dd'
        except:
            logger.error('Passed wrong argument / wrong date format - must be "yyyy-mm-dd"')
            sys.exit('Passed wrong argument / wrong date format - must be "yyyy-mm-dd"')
      
    try:
        engine = sqlalchemy.create_engine('postgresql://%s:%s@%s:5432/%s'
                                          %(options.username, options.password, 
                                            options.host_name, options.database_name), echo=False)           
        with open(sql_scripts_path + tiers_query_file, 'r') as reader:
            query = reader.read()
            _ = pd.read_sql_query(query, con=engine, params = {'end_date' : cur_date}) 
                       
        logger.info('Created a connection to the database %s with user %s at host %s'%(options.database_name, options.username, options.host_name))
    except:
        logger.error('Failed to create a connection to the SQL database')       
  
     
    logger.info('The model will run for : %s'%cur_date.isoformat())

# finding KPIs and scores for all customers for the passed date - cur_date
    df_scores = scores.get_scores(engine, cur_date.isoformat(), options.is_Bally, options.f_Output)   
# assigning tiers for all customers who are due on the passed date - cur_date
    _ = get_tiers(engine, df_scores, cur_date.isoformat(), options.is_Bally, options.f_Output)
     
    logger.info('--- The End ---')
    logger.info('-' * 80)

if __name__ == '__main__':
    main(sys.argv[1:])
    
