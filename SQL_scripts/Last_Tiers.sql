/*
* Created on February 15 2022
* @author:  Alexey Kotlik
 
  This script requires 1 arguments/parameters passed:
  	date - upper boundary of the data, last transaction to be accounted for 
  	
  Returns records of the latest assigned tiers for all customers up until the passed date
 */
WITH	
last_dates AS
(
	SELECT 
	customer_id,
	max(assigned_date) AS last_date
	FROM dat.ds_customer_tiers
	GROUP BY customer_id
	ORDER BY 1
)
SELECT 
cus_tiers.customer_id,
tier,
last_dates.last_date AS assigned_date
FROM last_dates
INNER JOIN dat.ds_customer_tiers AS cus_tiers ON (last_dates.customer_id = cus_tiers.customer_id) AND (last_dates.last_date <= (SELECT cast(%(end_date)s AS timestamp) + 86399 * interval '1 second')) AND (last_dates.last_date = cus_tiers.assigned_date)
ORDER BY 3 DESC
