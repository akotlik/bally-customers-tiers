/*
* Created on Oct 1 2021
* @author:  Alexey Kotlik
 
  This script requires 2 arguments/parameters passed:
  1 - company/casino. for now we only consider 'bally' or 'elite'. 
  	  in case we need to use it for another company - modify below CASE statement; 
  2 - date - upper boundary of the data, last transaction to be accounted for; 
 */
WITH 
cas_ids as (select (%s) as cas_id),
cas_codes as 
(
	select (case when cas_id = 'elite' then array[6000] else array[2000, 2020, 2040, 2080] 		end) as casino_ids	
	from cas_ids
 ),
-- convert array of casino codes into query
cas_ids_list as 
(
	select unnest(cas_codes.casino_ids) as casino_ids from cas_codes 
	),
/* the last date comes in date-only format so we have to
   modify the last date to include all transaction up until midnight of the previous day
*/ 	
last_dates as 
(
	select (cast((%s) as timestamp) -1 * interval '1 second') as last_date
	),
-- select customers for the particular casinos, who are not testing customers too	
sel_cas_custs as 
(
	SELECT customer_id 
	FROM dat.customer_dim AS c_dim
	INNER JOIN cas_ids_list
	ON cas_ids_list.casino_ids = c_dim.signup_casino_id
	WHERE testing_customer = False 
),
first_bet_dates AS
(
	SELECT
	m_bets.customer_id,
	MIN(m_bets.bet_date_min) AS first_bet_date
	FROM (
		SELECT
		sbk_cust.customer_id,
		(CASE WHEN
		 	  CASE  WHEN casino_id BETWEEN 6000 AND 6003 OR casino_id = 7030  OR casino_id = 2020 THEN CAST(to_char(bet_date at time zone 'US/Central', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					WHEN casino_id = 2000 OR casino_id = 6020 OR casino_id = 6021 OR casino_id = 6002 OR casino_id = 7010 THEN CAST(to_char(bet_date at time zone 'US/Mountain', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					WHEN casino_id = 2040 OR casino_id = 2080 OR casino_id = 7020 OR casino_id = 7000 THEN CAST(to_char(bet_date at time zone 'US/Eastern', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					ELSE CAST(to_char(bet_date at time zone 'US/Pacific', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					END =	MIN(CASE WHEN casino_id BETWEEN 6000 AND 6003 OR casino_id = 7030  OR casino_id = 2020 THEN CAST(to_char(bet_date at time zone 'US/Central', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									WHEN casino_id = 2000 OR casino_id = 6020 OR casino_id = 6021 OR casino_id = 6002 OR casino_id = 7010 THEN CAST(to_char(bet_date at time zone 'US/Mountain', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									WHEN casino_id = 2040 OR casino_id = 2080 OR casino_id = 7020 OR casino_id = 7000 THEN CAST(to_char(bet_date at time zone 'US/Eastern', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp) 
									ELSE CAST(to_char(bet_date at time zone 'US/Pacific', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									END)
					OVER(PARTITION BY sbk_cust.customer_id ORDER BY bet_date)
		 	 THEN bet_date
		 	 ELSE NULL
		 	 END) AS bet_date_min
		FROM dat.sbk_cust_bet_fac AS sbk_cust
		INNER JOIN sel_cas_custs AS cust_dim
		ON cust_dim.customer_id = sbk_cust.customer_id
		WHERE sbk_cust.bet_date <= (SELECT last_date FROM last_dates)
		) AS m_bets
	GROUP BY 1
),
-- define dates of the betting tranasctions and bet and payout KPIs from the beginning of activity for each customer
last_bet_dates AS
(
	SELECT
	l_bets.customer_id,
	MAX(l_bets.bet_date_max) AS last_bet_date,
	SUM(l_bets.bets_count) AS all_bets_count,
	SUM(l_bets.sports_turnover_w_fb_amt ) AS total_bet_amt,
	SUM(l_bets.sports_payout_w_fb_amt ) AS total_pay_amt
	FROM (
		SELECT
		sbk_cust.customer_id,
		sbk_cust.sports_bets_w_fb_cnt AS bets_count,
		sbk_cust.sports_turnover_w_fb_amt ,
		sbk_cust.sports_payout_w_fb_amt ,
		(CASE WHEN
		 	  CASE  WHEN casino_id BETWEEN 6000 AND 6003 OR casino_id = 7030  OR casino_id = 2020 THEN CAST(to_char(bet_date at time zone 'US/Central', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					WHEN casino_id = 2000 OR casino_id = 6020 OR casino_id = 6021 OR casino_id = 6002 OR casino_id = 7010 THEN CAST(to_char(bet_date at time zone 'US/Mountain', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					WHEN casino_id = 2040 OR casino_id = 2080 OR casino_id = 7020 OR casino_id = 7000 THEN CAST(to_char(bet_date at time zone 'US/Eastern', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					ELSE CAST(to_char(bet_date at time zone 'US/Pacific', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					END =	MAX(CASE WHEN casino_id BETWEEN 6000 AND 6003 OR casino_id = 7030  OR casino_id = 2020 THEN CAST(to_char(bet_date at time zone 'US/Central', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									WHEN casino_id = 2000 OR casino_id = 6020 OR casino_id = 6021 OR casino_id = 6002 OR casino_id = 7010 THEN CAST(to_char(bet_date at time zone 'US/Mountain', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									WHEN casino_id = 2040 OR casino_id = 2080 OR casino_id = 7020 OR casino_id = 7000 THEN CAST(to_char(bet_date at time zone 'US/Eastern', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp) 
									ELSE CAST(to_char(bet_date at time zone 'US/Pacific', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									END)
					OVER(PARTITION BY sbk_cust.customer_id ORDER BY bet_date)
		 	 THEN bet_date
		 	 ELSE NULL
		 	 END) AS bet_date_max
		FROM dat.sbk_cust_bet_fac sbk_cust
		INNER JOIN sel_cas_custs AS cust_dim
		ON cust_dim.customer_id = sbk_cust.customer_id
		WHERE sbk_cust.bet_date <= (SELECT last_date FROM last_dates)
		) AS l_bets
	GROUP BY 1
),
first_dep_dates AS
(
	SELECT
	f_deps.customer_id,
	MIN(f_deps.dep_date_min) AS first_dep_date
	FROM (
		SELECT
		txn_fac.customer_id,
		(CASE WHEN
		 	  CASE  WHEN casino_id BETWEEN 6000 AND 6003 OR casino_id = 7030  OR casino_id = 2020 THEN CAST(to_char(trans_date at time zone 'US/Central', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					WHEN casino_id = 2000 OR casino_id = 6020 OR casino_id = 6021 OR casino_id = 6002 OR casino_id = 7010 THEN CAST(to_char(trans_date at time zone 'US/Mountain', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					WHEN casino_id = 2040 OR casino_id = 2080 OR casino_id = 7020 OR casino_id = 7000 THEN CAST(to_char(trans_date at time zone 'US/Eastern', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					ELSE CAST(to_char(trans_date at time zone 'US/Pacific', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					END =	MIN(CASE WHEN casino_id BETWEEN 6000 AND 6003 OR casino_id = 7030  OR casino_id = 2020 THEN CAST(to_char(trans_date at time zone 'US/Central', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									WHEN casino_id = 2000 OR casino_id = 6020 OR casino_id = 6021 OR casino_id = 6002 OR casino_id = 7010 THEN CAST(to_char(trans_date at time zone 'US/Mountain', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									WHEN casino_id = 2040 OR casino_id = 2080 OR casino_id = 7020 OR casino_id = 7000 THEN CAST(to_char(trans_date at time zone 'US/Eastern', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp) 
									ELSE CAST(to_char(trans_date at time zone 'US/Pacific', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									END)
					OVER(PARTITION BY txn_fac.customer_id ORDER BY trans_date)
		 	 THEN trans_date
		 	 ELSE NULL
		 	 END) AS dep_date_min
		FROM dat.eco_cust_all_trans_fac AS txn_fac
		INNER JOIN sel_cas_custs AS cust_dim
		ON cust_dim.customer_id = txn_fac.customer_id
		WHERE txn_fac.trans_date <= (SELECT last_date FROM last_dates)
		) AS f_deps
	GROUP BY 1
),
-- define dates of the deposit tranasctions and deposits and withdrawals KPIs from the beginning of activity for each customer
last_dep_dates AS
(
	SELECT
	l_deps.customer_id,
	SUM(l_deps.withdrawal_amt) AS total_wthdrl_amt,
	SUM(l_deps.deposit_amt) AS total_dep_amount,
	(SUM(l_deps.deposit_amt) - SUM(l_deps.withdrawal_amt)) AS total_net_deposit,
	MAX(l_deps.dep_date_max) AS last_dep_date
	FROM (
		SELECT
		txn_fac.customer_id,
		(CASE WHEN txn_fac.trans_type_id IN (1, 2) THEN txn_fac.amount  ELSE 0 END) AS deposit_amt,
		(CASE WHEN txn_fac.trans_type_id IN (11, 22) THEN txn_fac.amount ELSE 0 END) AS withdrawal_amt,
		(CASE WHEN
		 	  CASE  WHEN casino_id BETWEEN 6000 AND 6003 OR casino_id = 7030  OR casino_id = 2020 THEN CAST(to_char(trans_date at time zone 'US/Central', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					WHEN casino_id = 2000 OR casino_id = 6020 OR casino_id = 6021 OR casino_id = 6002 OR casino_id = 7010 THEN CAST(to_char(trans_date at time zone 'US/Mountain', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					WHEN casino_id = 2040 OR casino_id = 2080 OR casino_id = 7020 OR casino_id = 7000 THEN CAST(to_char(trans_date at time zone 'US/Eastern', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					ELSE CAST(to_char(trans_date at time zone 'US/Pacific', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
					END =	MAX(CASE WHEN casino_id BETWEEN 6000 AND 6003 OR casino_id = 7030  OR casino_id = 2020 THEN CAST(to_char(trans_date at time zone 'US/Central', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									WHEN casino_id = 2000 OR casino_id = 6020 OR casino_id = 6021 OR casino_id = 6002 OR casino_id = 7010 THEN CAST(to_char(trans_date at time zone 'US/Mountain', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									WHEN casino_id = 2040 OR casino_id = 2080 OR casino_id = 7020 OR casino_id = 7000 THEN CAST(to_char(trans_date at time zone 'US/Eastern', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp) 
									ELSE CAST(to_char(trans_date at time zone 'US/Pacific', 'MM/DD/YYYY HH12:MI:SS AM TZ')  AS timestamp)
									END)
					OVER(PARTITION BY txn_fac.customer_id ORDER BY trans_date)
		 	 THEN trans_date
		 	 ELSE NULL
		 	 END) AS dep_date_max
		FROM dat.eco_cust_all_trans_fac AS txn_fac
		INNER JOIN sel_cas_custs AS cust_dim
		ON cust_dim.customer_id = txn_fac.customer_id
		WHERE txn_fac.trans_date <= (SELECT last_date FROM last_dates)
		) AS l_deps
	GROUP BY 1
),
-- define dates of the tranasctions and some KPIs for the whole period 
trans_dates AS 
(
	SELECT
	t_dates.customer_id,
	t_dates.first_bet_date,
	t_dates.last_bet_date,
	t_dates.first_dep_date,
	t_dates.last_dep_date,
	t_dates.all_bets_count,
	t_dates.total_bet_amt,
	t_dates.total_pay_amt,
	t_dates.total_wthdrl_amt,
	t_dates.total_dep_amount,
	t_dates.total_net_deposit
	FROM (
			SELECT
			first_bet_dates.customer_id,
			first_bet_dates.first_bet_date,
			last_bet_dates.last_bet_date,
			first_dep_dates.first_dep_date,
			last_dep_dates.last_dep_date,
			last_bet_dates.all_bets_count,
			(last_bet_dates.total_bet_amt /100) AS total_bet_amt,
			(last_bet_dates.total_pay_amt /100) as total_pay_amt,
			(last_dep_dates.total_wthdrl_amt /100) AS total_wthdrl_amt,
			(last_dep_dates.total_dep_amount /100) AS total_dep_amount,
			(last_dep_dates.total_net_deposit /100) AS total_net_deposit
			FROM last_bet_dates
			FULL JOIN first_bet_dates
			ON first_bet_dates.customer_id = last_bet_dates.customer_id
			FULL JOIN first_dep_dates
			ON first_bet_dates.customer_id = first_dep_dates.customer_id
			FULL JOIN last_dep_dates
			ON first_bet_dates.customer_id = last_dep_dates.customer_id
		) AS t_dates
),
-- define first and last transaction dates for each customer
trans_data AS
(
	SELECT 
	*,
	LEAST(trans_dates.first_bet_date, trans_dates.first_dep_date) AS first_trans,
	GREATEST (trans_dates.last_bet_date, trans_dates.last_dep_date) AS last_trans
	FROM trans_dates
	),
/* we need to define 12 last 30-days intervals
  to do that we first find number of months of the customers lifetime;
  then we project last possible date of activity by multiplying 
  number of months * 30 and adding to the fisrst transaction date. 
  the result might in the future in relation to the last date recieved via parameters passed to the script;
  and then we look back defining dates for all 12 periods.  
 */	
intervals AS
(
	SELECT
	transaction_data.*,
	CAST(CAST((first_trans + interval '1 day' * (30 * transaction_data.num_months)) as date) || ' 23:59:59' AS timestamp) AS last_date_for_tiers,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 1))) as date)  || ' 00:00:00' AS timestamp) AS date_12_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 2))) as date)  || ' 00:00:00' AS timestamp) AS date_11_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 3))) as date)  || ' 00:00:00' AS timestamp) AS date_10_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 4))) as date)  || ' 00:00:00' AS timestamp) AS date_9_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 5))) as date)  || ' 00:00:00' AS timestamp) AS date_8_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 6))) as date)  || ' 00:00:00' AS timestamp) AS date_7_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 7))) as date)  || ' 00:00:00' AS timestamp) AS date_6_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 8))) as date)  || ' 00:00:00' AS timestamp) AS date_5_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 9))) as date)  || ' 00:00:00' AS timestamp) AS date_4_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 10))) as date)  || ' 00:00:00' AS timestamp) AS date_3_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 11))) as date)  || ' 00:00:00' AS timestamp) AS date_2_month,
	CAST(CAST((first_trans + interval '1 day' * (30 * (transaction_data.num_months - 12))) as date)  || ' 00:00:00' AS timestamp) AS date_1_month
	FROM (
			SELECT
			*,
			(CAST(trans_data.last_trans AS date) - CAST(trans_data.first_trans AS date) + 1) AS num_days,
			CASE WHEN (CAST(trans_data.last_trans AS date) - CAST(trans_data.first_trans AS date))<1 THEN 1
				 WHEN (interval '1 day' * (30 * CEIL(CAST(CAST(trans_data.last_trans AS date) - CAST(trans_data.first_trans AS date) AS FLOAT) /30)) + CAST(trans_data.first_trans AS date)) < (SELECT last_date FROM last_dates) 
				 	THEN CEIL(CAST(CAST(trans_data.last_trans AS date) - CAST(trans_data.first_trans AS date) AS FLOAT) /30) 
				 ELSE ROUND(CAST(CAST(trans_data.last_trans AS date) - CAST(trans_data.first_trans AS date) AS FLOAT) /30)
				 END  AS num_months,
			DATE_PART('day', (SELECT last_date FROM last_dates) - trans_data.last_trans) AS days_since_last_trans
			FROM trans_data
		) AS transaction_data
	WHERE (days_since_last_trans <= 360)
	order by days_since_last_trans DESC, num_months DESC
),
-- define betting KPIs for each period
bet_KPI_per_period AS	
(
	SELECT
	intervals.customer_id,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_12_month) AND (bet_dates.bet_date < intervals.last_date_for_tiers) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_12,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_11_month) AND (bet_dates.bet_date < intervals.date_12_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_11,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_10_month) AND (bet_dates.bet_date < intervals.date_11_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_10,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_9_month) AND (bet_dates.bet_date < intervals.date_10_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_9,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_8_month) AND (bet_dates.bet_date < intervals.date_9_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_8,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_7_month) AND (bet_dates.bet_date < intervals.date_8_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_7,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_6_month) AND (bet_dates.bet_date < intervals.date_7_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_6,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_5_month) AND (bet_dates.bet_date < intervals.date_6_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_5,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_4_month) AND (bet_dates.bet_date < intervals.date_5_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_4,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_3_month) AND (bet_dates.bet_date < intervals.date_4_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_3,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_2_month) AND (bet_dates.bet_date < intervals.date_3_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_2,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_1_month) AND (bet_dates.bet_date < intervals.date_2_month) THEN bet_dates.bet_days_cnt ELSE 0 END ) AS month_bet_days_cnt_1,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_12_month) AND (bet_dates.bet_date < intervals.last_date_for_tiers) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_12,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_11_month) AND (bet_dates.bet_date < intervals.date_12_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_11,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_10_month) AND (bet_dates.bet_date < intervals.date_11_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_10,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_9_month) AND (bet_dates.bet_date < intervals.date_10_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_9,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_8_month) AND (bet_dates.bet_date < intervals.date_9_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_8,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_7_month) AND (bet_dates.bet_date < intervals.date_8_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_7,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_6_month) AND (bet_dates.bet_date < intervals.date_7_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_6,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_5_month) AND (bet_dates.bet_date < intervals.date_6_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_5,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_4_month) AND (bet_dates.bet_date < intervals.date_5_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_4,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_3_month) AND (bet_dates.bet_date < intervals.date_4_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_3,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_2_month) AND (bet_dates.bet_date < intervals.date_3_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_2,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_1_month) AND (bet_dates.bet_date < intervals.date_2_month) THEN bet_dates.sports_bet_amt ELSE 0 END ) AS month_bets_amount_1,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_12_month) AND (bet_dates.bet_date < intervals.last_date_for_tiers) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_12,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_11_month) AND (bet_dates.bet_date < intervals.date_12_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_11,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_10_month) AND (bet_dates.bet_date < intervals.date_11_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_10,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_9_month) AND (bet_dates.bet_date < intervals.date_10_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_9,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_8_month) AND (bet_dates.bet_date < intervals.date_9_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_8,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_7_month) AND (bet_dates.bet_date < intervals.date_8_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_7,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_6_month) AND (bet_dates.bet_date < intervals.date_7_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_6,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_5_month) AND (bet_dates.bet_date < intervals.date_6_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_5,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_4_month) AND (bet_dates.bet_date < intervals.date_5_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_4,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_3_month) AND (bet_dates.bet_date < intervals.date_4_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_3,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_2_month) AND (bet_dates.bet_date < intervals.date_3_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_2,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_1_month) AND (bet_dates.bet_date < intervals.date_2_month) THEN bet_dates.daily_bets_cnt ELSE 0 END ) AS month_bets_count_1,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_12_month) AND (bet_dates.bet_date < intervals.last_date_for_tiers) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_12,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_11_month) AND (bet_dates.bet_date < intervals.date_12_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_11,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_10_month) AND (bet_dates.bet_date < intervals.date_11_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_10,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_9_month) AND (bet_dates.bet_date < intervals.date_10_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_9,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_8_month) AND (bet_dates.bet_date < intervals.date_9_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_8,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_7_month) AND (bet_dates.bet_date < intervals.date_8_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_7,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_6_month) AND (bet_dates.bet_date < intervals.date_7_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_6,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_5_month) AND (bet_dates.bet_date < intervals.date_6_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_5,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_4_month) AND (bet_dates.bet_date < intervals.date_5_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_4,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_3_month) AND (bet_dates.bet_date < intervals.date_4_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_3,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_2_month) AND (bet_dates.bet_date < intervals.date_3_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_2,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_1_month) AND (bet_dates.bet_date < intervals.date_2_month) THEN bet_dates.daily_bets_w_fb_cnt ELSE 0 END ) AS month_all_bets_count_1,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_12_month) AND (bet_dates.bet_date < intervals.last_date_for_tiers) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_12,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_11_month) AND (bet_dates.bet_date < intervals.date_12_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_11,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_10_month) AND (bet_dates.bet_date < intervals.date_11_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_10,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_9_month) AND (bet_dates.bet_date < intervals.date_10_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_9,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_8_month) AND (bet_dates.bet_date < intervals.date_9_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_8,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_7_month) AND (bet_dates.bet_date < intervals.date_8_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_7,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_6_month) AND (bet_dates.bet_date < intervals.date_7_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_6,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_5_month) AND (bet_dates.bet_date < intervals.date_6_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_5,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_4_month) AND (bet_dates.bet_date < intervals.date_5_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_4,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_3_month) AND (bet_dates.bet_date < intervals.date_4_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_3,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_2_month) AND (bet_dates.bet_date < intervals.date_3_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_2,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_1_month) AND (bet_dates.bet_date < intervals.date_2_month) THEN bet_dates.win_counts ELSE 0 END ) AS month_win_counts_1,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_12_month) AND (bet_dates.bet_date < intervals.last_date_for_tiers) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_12,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_11_month) AND (bet_dates.bet_date < intervals.date_12_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_11,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_10_month) AND (bet_dates.bet_date < intervals.date_11_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_10,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_9_month) AND (bet_dates.bet_date < intervals.date_10_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_9,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_8_month) AND (bet_dates.bet_date < intervals.date_9_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_8,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_7_month) AND (bet_dates.bet_date < intervals.date_8_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_7,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_6_month) AND (bet_dates.bet_date < intervals.date_7_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_6,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_5_month) AND (bet_dates.bet_date < intervals.date_6_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_5,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_4_month) AND (bet_dates.bet_date < intervals.date_5_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_4,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_3_month) AND (bet_dates.bet_date < intervals.date_4_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_3,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_2_month) AND (bet_dates.bet_date < intervals.date_3_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_2,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_1_month) AND (bet_dates.bet_date < intervals.date_2_month) THEN bet_dates.payout_amt ELSE 0 END ) AS month_payout_amt_1,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_12_month) AND (bet_dates.bet_date < intervals.last_date_for_tiers) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_12,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_11_month) AND (bet_dates.bet_date < intervals.date_12_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_11,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_10_month) AND (bet_dates.bet_date < intervals.date_11_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_10,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_9_month) AND (bet_dates.bet_date < intervals.date_10_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_9,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_8_month) AND (bet_dates.bet_date < intervals.date_9_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_8,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_7_month) AND (bet_dates.bet_date < intervals.date_8_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_7,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_6_month) AND (bet_dates.bet_date < intervals.date_7_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_6,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_5_month) AND (bet_dates.bet_date < intervals.date_6_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_5,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_4_month) AND (bet_dates.bet_date < intervals.date_5_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_4,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_3_month) AND (bet_dates.bet_date < intervals.date_4_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_3,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_2_month) AND (bet_dates.bet_date < intervals.date_3_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_2,
	SUM(CASE WHEN (bet_dates.bet_date >= intervals.date_1_month) AND (bet_dates.bet_date < intervals.date_2_month) THEN bet_dates.payout_w_fb_amt ELSE 0 END ) AS month_payout_w_fb_amt_1
	FROM intervals
	INNER JOIN 
		(
		SELECT 
		sbk_cust.customer_id,
		CAST(sbk_cust.bet_date AS date) AS bet_date,
		COUNT(DISTINCT CAST(sbk_cust.bet_date AS date)) as bet_days_cnt,
		SUM(sbk_cust.bet_amt / 100) AS sports_bet_amt,
		SUM(sbk_cust.sports_bets_cnt) AS daily_bets_cnt,
		SUM(sbk_cust.sports_bets_w_fb_cnt) AS daily_bets_w_fb_cnt,
		SUM(CASE WHEN sbk_cust.grading_result_id = 1 THEN 1 ELSE 0 END) AS win_counts,
		SUM(sbk_cust.sports_payout_amt / 100) AS payout_amt,
		SUM(sbk_cust.sports_payout_w_fb_amt / 100) AS payout_w_fb_amt
		FROM dat.sbk_cust_bet_fac AS sbk_cust
		GROUP BY sbk_cust.customer_id, CAST(sbk_cust.bet_date AS date)
		) AS bet_dates
	ON intervals.customer_id = bet_dates.customer_id
	GROUP BY 1
),
-- define monthly deposits and withdrawals
dep_KPI_per_period AS	
(
	SELECT
	intervals.customer_id AS dep_customer_id,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_12_month) AND (dep_dates.dep_date < intervals.last_date_for_tiers) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_12,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_11_month) AND (dep_dates.dep_date < intervals.date_12_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_11,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_10_month) AND (dep_dates.dep_date < intervals.date_11_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_10,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_9_month) AND (dep_dates.dep_date < intervals.date_10_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_9,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_8_month) AND (dep_dates.dep_date < intervals.date_9_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_8,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_7_month) AND (dep_dates.dep_date < intervals.date_8_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_7,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_6_month) AND (dep_dates.dep_date < intervals.date_7_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_6,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_5_month) AND (dep_dates.dep_date < intervals.date_6_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_5,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_4_month) AND (dep_dates.dep_date < intervals.date_5_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_4,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_3_month) AND (dep_dates.dep_date < intervals.date_4_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_3,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_2_month) AND (dep_dates.dep_date < intervals.date_3_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_2,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_1_month) AND (dep_dates.dep_date < intervals.date_2_month) THEN dep_dates.deposit_amt ELSE 0 END ) AS month_deposits_amount_1,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_12_month) AND (dep_dates.dep_date < intervals.last_date_for_tiers) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_12,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_11_month) AND (dep_dates.dep_date < intervals.date_12_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_11,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_10_month) AND (dep_dates.dep_date < intervals.date_11_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_10,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_9_month) AND (dep_dates.dep_date < intervals.date_10_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_9,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_8_month) AND (dep_dates.dep_date < intervals.date_9_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_8,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_7_month) AND (dep_dates.dep_date < intervals.date_8_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_7,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_6_month) AND (dep_dates.dep_date < intervals.date_7_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_6,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_5_month) AND (dep_dates.dep_date < intervals.date_6_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_5,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_4_month) AND (dep_dates.dep_date < intervals.date_5_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_4,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_3_month) AND (dep_dates.dep_date < intervals.date_4_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_3,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_2_month) AND (dep_dates.dep_date < intervals.date_3_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_2,
	SUM(CASE WHEN (dep_dates.dep_date >= intervals.date_1_month) AND (dep_dates.dep_date < intervals.date_2_month) THEN dep_dates.withdrawal_amt ELSE 0 END ) AS month_withdrawal_amt_1
	FROM intervals
	INNER JOIN 
		(	
		SELECT 
		txn_fac.customer_id,
		CAST(txn_fac.trans_date AS date) AS dep_date,
		SUM(CASE WHEN txn_fac.trans_type_id IN (1, 2) THEN txn_fac.amount / 100 ELSE 0 END) AS deposit_amt,
		SUM(CASE WHEN txn_fac.trans_type_id IN (11, 22) THEN txn_fac.amount / 100 ELSE 0 END) AS withdrawal_amt
		FROM dat.eco_cust_all_trans_fac AS txn_fac
		GROUP BY txn_fac.customer_id, CAST(txn_fac.trans_date AS date)
		) AS dep_dates
	ON intervals.customer_id = dep_dates.customer_id
	GROUP BY 1 
),
-- how many active weeks each customer had over the last 12 30-days periods
active_weeks AS
(
	SELECT 
	customer_id,
	COUNT(*) AS num_active_weeks
	FROM 
		(	SELECT 
			sbk_cust.customer_id,
			date_trunc('week', sbk_cust.bet_date),
			SUM(sbk_cust.sports_bets_cnt)
			FROM dat.sbk_cust_bet_fac AS sbk_cust
			FULL JOIN intervals
			ON intervals.customer_id = sbk_cust.customer_id
			WHERE ((sbk_cust.bet_date >= intervals.date_1_month) AND (sbk_cust.bet_date <= intervals.last_date_for_tiers)) 
			GROUP BY 1, 2
			ORDER BY 1, 2 
		) AS weekly
	GROUP BY 1
	ORDER BY 1 
)
SELECT 
intervals.first_trans,
intervals.last_trans,
intervals.num_days,
intervals.num_months,
active_weeks.num_active_weeks,
intervals.days_since_last_trans,
intervals.last_date_for_tiers,
intervals.all_bets_count,
intervals.total_bet_amt,
intervals.total_pay_amt,
intervals.total_wthdrl_amt,
intervals.total_dep_amount,
intervals.total_net_deposit,
bet_KPI_per_period.*,
dep_KPI_per_period.*
FROM bet_KPI_per_period
LEFT JOIN intervals
ON intervals.customer_id = bet_KPI_per_period.customer_id
LEFT JOIN dep_KPI_per_period
ON intervals.customer_id = dep_KPI_per_period.dep_customer_id
LEFT JOIN active_weeks
ON intervals.customer_id = active_weeks.customer_id
