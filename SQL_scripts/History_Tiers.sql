/*
* Created on June 10 2022
* @author:  Alexey Kotlik
 
  This script requires 1 arguments/parameters passed:
  	date - upper boundary of the data, last transaction to be accounted for 
  	
  Returns all records of assigned tiers for all customers up until the passed date
 */
WITH 
last_dates as 
(	
	select (cast(%(end_date)s AS timestamp) -1 * interval '1 second') AS cur_date
	)
SELECT 
cus_tiers.customer_id,
tier,
assigned_date 
--assigned_date as updated_date
FROM dat.ds_customer_tiers AS cus_tiers 
WHERE assigned_date < (SELECT cur_date FROM last_dates)
ORDER BY 1, 3 DESC


