# -*- coding: utf-8 -*-
"""
Created on Apr 12 2022

@author: akotlik
"""

import numpy as np
import pandas as pd
import scipy
import logging


# Utility functions   
def get_centroids(loc_df, clusters_col, loc_KPIs_cols, N_clusters = 6):
    '''
    Provides coordinates of the clusters centers

    Parameters
    ----------
    loc_df : pandas dataframe
        data to be analyzed
    clusters_col : string 
        name of the column with assigned tiers
    loc_KPIs_cols : list of strings
        names of the columns with KPIs
    N_clusters : integer
        number of clusters. The default is 6.

    Returns
    -------
    centers : numpy array 
        coordinates of the center of each cluster
    '''
    centers = np.zeros([N_clusters, len(loc_KPIs_cols)])
    for i in range(N_clusters):
      centers[i] = loc_df.loc[loc_df[clusters_col] == i][loc_KPIs_cols].mean(axis = 0).values
    return centers
   

def find_nearest(cur_customer, loc_KPI_columns, loc_centers):
    '''
    Finds nearest cluster for each customer

    Parameters
    ----------
    cur_customer : pandas sequence with all KPIs
    loc_KPI_columns : list of strings
        names of KPI columns used to define the coordinates.
    loc_centers : numpy array 
        coordinates of centers of each cluster.

    Returns
    -------
    new_cluster : integer
    '''
    cur_coords = np.reshape(cur_customer[loc_KPI_columns].values, (1,-1))
    new_cluster = np.argmin(scipy.spatial.distance.cdist(cur_coords, loc_centers))
    
    # if a customer is relatively new and gets assigned a VIP tier then we need 
    # to be cautious, and assign him Tier 3 until he maintains for 90 days that 
    # he is really a VIP client
    if (new_cluster >= 4) & (cur_customer['num_days'] < 90):
        new_cluster = 3
    return new_cluster


def inertia(cur_cluster):
    '''
    Finds Iertia for each cluster

    Parameters
    ----------
    cur_cluster : numpy array of floats - Number of Customers * Number of KPIs

    Returns
    -------
    Inertia - float
    '''
    center = np.reshape(cur_cluster.mean(axis = 0), (1, -1))
    distances = scipy.spatial.distance.cdist(cur_cluster, center)
    d2 = np.square(distances)
    return np.sqrt(np.sum(d2))   


def tiers_comparison(loc_df, feature_name, cluster_name, N_clusters):
    '''
    Prints out basic stats for each KPI per cluster

    Parameters
    ----------
    loc_df : pandas sequence with all KPIs
    feature_name : string
        name of the columns of KPI
    cluster_name : string
        name of the column with assigned tiers
    N_clusters : integer
    f_Print : boolean, optional
        flag - do we want to print out debugging info. The default is False.

    Returns
    -------
    None.

    '''
    logger = logging.getLogger('customer_tiers')

    df_temp = loc_df.loc[loc_df[cluster_name] == 0]
    df_tier = pd.DataFrame(df_temp[feature_name].describe()).rename(columns={feature_name:0})
  
    for i in range(N_clusters-1):
      df_temp = loc_df.loc[loc_df[cluster_name] == i+1]
      df_tier[i+1] = df_temp[feature_name].describe()

    logger.info(f'KPI : {feature_name}')
    logger.info(df_tier)
    logger.info('-' * 80)
