"""
Created on Nov 10 2021

@author: akotlik
"""
from datetime import date
import numpy as np
import pandas as pd
import random
import logging

sql_scripts_path = './SQL_scripts/'   
data_path = './data/'


# Utility functions
def last_3_months(row):
    '''
    Function to find indexes of last 3 active periods (if any)

    Parameters
    ----------
    row : list of 12 integers, reflect if there was any activity in each months.

    Returns
    -------
    active3 : list of indexes of active months
    '''
    actives = row > 0
    counter = 0
    active3 = []
    i = 0
    while counter < 3:
        if i == 11:
            break
        elif actives[i]:
            active3.append(i)
            counter += 1
        i += 1 
    return active3      


def get_KPI(cur, bet_days_counts_columns, bet_amounts_columns, 
            bets_counts_columns, bets_all_counts_columns, win_counts_columns, 
            payout_amount_columns, payout_amount_w_fb_columns, 
            deposit_columns, withdrawal_columns, df):
    '''
    Function to retrive KPIs from each customer for each active period, 
    applied per row

    Parameters
    ----------
    cur : current row of the dataframe, passed by default by apply method
    bet_days_counts_columns : list of names of columns for each month 
    bet_amounts_columns : list of names of columns for each month 
    bets_counts_columns : list of names of columns for each month 
    bets_all_counts_columns : list of names of columns for each month 
    win_counts_columns : list of names of columns for each month 
    payout_amount_columns : list of names of columns for each month 
    payout_amount_w_fb_columns : list of names of columns for each month 
    deposit_columns : list of names of columns for each month 
    withdrawal_columns : list of names of columns for each month 
    df : pandas dataframe which contains all KPIs for all 12 months 

    Returns
    -------
    cur_kpi : list of all KPIs for current row - active period of the current customer

    '''
    ind = int(cur.period) # period is an index of the active month for which we retrieve each KPI
    bet_days_counts_column = bet_days_counts_columns[ind]
    bet_amounts_column = bet_amounts_columns[ind]
    bets_counts_column = bets_counts_columns[ind]
    bets_all_counts_column = bets_all_counts_columns[ind]
    win_counts_column = win_counts_columns[ind]
    payout_amount_column = payout_amount_columns[ind]
    payout_amount_w_fb_column = payout_amount_w_fb_columns[ind]
    deposit_column = deposit_columns[ind]
    withdrawal_column = withdrawal_columns[ind]
    
    cur_kpi = df.loc[cur.name, [bet_days_counts_column, bet_amounts_column, bets_counts_column, 
                                bets_all_counts_column, win_counts_column, 
                                payout_amount_column, payout_amount_w_fb_column,
                                deposit_column, withdrawal_column
                                ]]
    
    net_deposit_amount = cur_kpi.loc[deposit_column] - cur_kpi.loc[withdrawal_column]
    avg_bet_amount = round(cur_kpi[bet_amounts_column] / (cur_kpi[bets_all_counts_column] + 0.00000001), 1)
    avg_bet_per_visit = round(cur_kpi[bets_counts_column] / (cur_kpi[bet_days_counts_column] + 0.00000001) , 1)
    win_ratio = round(cur_kpi[win_counts_column] / (cur_kpi[bets_all_counts_column] + 0.00000001) , 2)
    cur_kpi = np.append(cur_kpi, [net_deposit_amount, avg_bet_amount, avg_bet_per_visit, win_ratio 
                                  ])
    return cur_kpi


def scoring_KPI(x, scores, pct):
    '''
    Assigning score to each KPI based on the percentiles

    Parameters
    ----------
    x : float
        KPI value
    scores : list of integers
        possible scores
    pct : list of integers
        percentiles thresholds

    Returns
    -------
    score : integer
    '''
    score = scores[len(scores)-1]
    for i in range(len(scores)):
        if x <= pct[i]:
            score = scores[i]
            break
   
    return score
# -----------------------------------------------------


def get_scores(engine, cur_date, 
               is_Bally = True, f_Output = False):
    '''
    Main function, retrieves KPIs from the SQL database. It also assings scores
    based on the predfined percentiles for initial customers analysis and clusterization.

    Parameters
    ----------
    engine : SQLAlchemy object, required
    cur_date : string representing date in yyyy-mm-dd format, required
         given date  for computing current KPI and tiers
    is_Bally : boolean, optional
        flag - do we use Bally's data or Elite's. The default is True for Bally
    f_Output : boolean, optional
        flag - do we need to store tiers and KPIs to csv files
        The default is False.
 
    Returns
    -------
    df_KPI_3periods_avg : pandas dataframe
        Contains KPIs and scores for each active period of each 
        customer (max 3 rows for a customer if the customer had 3 active periods).

    '''
    logger = logging.getLogger('customer_tiers')

    SEED = 1970
    random.seed(SEED)
    cur_date_only = date.fromisoformat(cur_date)
        
    bet_query_file = 'Bets_Deps_params.sql'
    
    # if we ever to decide to use balance-related KPIs then we need to use 
    # another SQL query, and then merge the results into the dataframe:
    ## bal_query_file = 'balances_beg_end_month_params.sql'
    
    
    if is_Bally:
        company = 'bally'
    else:
        company = 'elite'
                
    with open(sql_scripts_path + bet_query_file, 'r') as reader:
        query = reader.read()
        df_all = pd.read_sql_query(query, con=engine, params = (company, cur_date))
    
    df_all.set_index('customer_id', inplace = True)  
    df = df_all.copy()
    df.drop(['dep_customer_id'], axis=1, inplace=True)
    
    logger.info('Date : ' + cur_date)
    logger.info('Total number of customers across all states : %i'%df.shape[0])

    df['due'] = df.first_trans.apply(lambda x: (cur_date_only - x.date()).days % 30)
    
    #fill NaN for the customers with missing records from the balances table
    df.total_dep_amount.fillna(0.1, inplace = True)
    df.total_net_deposit.fillna(0.1, inplace = True)
    df.fillna(0, inplace = True)
        
    logger.info('Number of customers with no active weeks (an ERROR if not 0) : %i'%df.num_active_weeks.isnull().sum())
    logger.info('Number of customers with no deposits but with some withdrawals : %i'%df.loc[df.total_dep_amount == 0].shape[0])   
    
    # to avoid division by 0 lets set total net deposit to 1
    df.loc[df.total_dep_amount == 0, 'total_dep_amount'] = 1     
    df['total_dep_relative'] = df.total_net_deposit / df.total_dep_amount
    

    logger.info('Number of customers with 0 deposits still (an ERROR if not 0) : %i'%df.loc[df.total_dep_amount == 0].shape[0])
    logger.info('Number of customers with no deposits AND no withdrawals: %i'%df.loc[df.total_dep_amount == 0.1].shape[0])
          
    df['recent_activity_freq'] = df.num_active_weeks / (df.num_days / 7)
    df['total_net_deposit_per_bet'] = df.total_net_deposit / df.all_bets_count 
    columns = df.columns.tolist()
    new_columns = columns[:13] + columns[-3:]  + [columns[-4]] + columns[13:-4]
          
    df = df[new_columns]         
    KPI_columns = new_columns[17:]
          
    bet_days_counts_columns = KPI_columns[:12]
    bet_amounts_columns = KPI_columns[12:24]
    bets_counts_columns = KPI_columns[24:36]
    bets_all_counts_columns = KPI_columns[36:48]
    win_counts_columns = KPI_columns[48:60]
    payout_amount_columns = KPI_columns[60:72]
    payout_amount_w_fb_columns = KPI_columns[72:84]
    deposit_columns = KPI_columns[84:96]
    withdrawal_columns = KPI_columns[96:]
    periods_list = ['mon%i'%i for i in range(12) ]
          
    logger.info('-' * 80)
    logger.info(df['days_since_last_trans'].describe())
    logger.info('-' * 80)
    
    all_columns = df.columns.tolist()
    
    df_KPI = df[all_columns[:17]].copy()
    # if we consider any activity to determine if the customer was active during a certain period
    df_active_periods = df.copy()
    df_active_periods[periods_list] = df_active_periods[bets_all_counts_columns].values + df_active_periods[deposit_columns].values + df_active_periods[withdrawal_columns].values
    df_KPI['active_periods'] = df_active_periods[periods_list].apply(lambda x: np.sum(x > 0), axis = 1)

    logger.info('Number of customers with 0 active periods : %i'%df_KPI.loc[df_KPI['active_periods']==0].shape[0])
    
    df_KPI['period'] = None
    # if we consider any activity to determine if the customer was active during a certain period
    df_KPI['period'] = df_active_periods[periods_list].apply(last_3_months, axis = 1)
    # if we consider only betting activity
    # df_KPI['period'] = df[bet_amounts_columns].apply(last_3_months, axis = 1)
    
    df_KPI_exp = df_KPI.explode('period')
    df_KPI_3periods = df_KPI_exp.copy()

    logger.info(df_KPI_3periods.shape)
    
    period_KPIs = ['bet_days_counts', 'bet_amounts', 'bets_counts', 
                   'bets_all_counts', 'win_counts',  'payout_amount', 
                   'payout_amount_w_fb', 'deposit_amounts', 
                   'withdrawal_amounts', 'net_deposits', 'avg_bet_amount', 
                   'avg_bet_per_visit', 'win_ratio'
                  ]   
    df_KPI_3periods.dropna(inplace = True)

    logger.info('Active periods for all customers %i'%df_KPI_3periods.shape[0])
    
    df_KPI_3periods[period_KPIs] = 0
    df_KPI_3periods[period_KPIs] = df_KPI_3periods.apply(
        get_KPI, args=(bet_days_counts_columns, bet_amounts_columns, 
                       bets_counts_columns, bets_all_counts_columns, 
                       win_counts_columns, payout_amount_columns, 
                       payout_amount_w_fb_columns, deposit_columns, 
                       withdrawal_columns, df), axis = 1, result_type = 'expand')   
    
    percentiles = [10, 25, 50, 75, 90]
    df_KPI_unique_ids = df_KPI_3periods.fillna(0)
    bet_days_counts_pct = np.percentile(df_KPI_unique_ids.bet_days_counts.values, percentiles)
    ap_pct = np.percentile(df_KPI_unique_ids.active_periods.values, percentiles)
    naw_pct = np.percentile(df_KPI_unique_ids.num_active_weeks.values, percentiles)
    avg_bet_amount_pct = np.percentile(df_KPI_unique_ids.avg_bet_amount.values, percentiles)
    bets_counts_pct = np.percentile(df_KPI_unique_ids.bets_counts.values, percentiles)
    avg_bet_per_visit_pct = np.percentile(df_KPI_unique_ids.avg_bet_per_visit.values, percentiles)
    win_ratio_pct = np.percentile(df_KPI_unique_ids.win_ratio.values, percentiles)
    active_weeks_ratio_pct = np.percentile(df_KPI_unique_ids.recent_activity_freq.values, percentiles)
    
    total_net_deposit_pct = np.percentile(df_KPI_unique_ids.total_net_deposit.values, [1, 5, 10, 25, 75, 90, 95])
    total_dep_relative_pct = np.percentile(df_KPI_unique_ids.total_dep_relative.values, [1, 5, 10, 15, 25, 35, 45, 50])
    deposit_amounts_pct = np.percentile(df_KPI_unique_ids.deposit_amounts.values, [25, 50, 75, 90, 95, 99])
    withdrawal_amounts_pct = np.percentile(df_KPI_unique_ids.withdrawal_amounts.values, [75, 90, 95, 99])
    net_deposit_amount_pct = np.percentile(df_KPI_unique_ids.net_deposits.values, [1, 5, 50, 75, 90, 95, 99])
    net_deposit_per_bet_pct = np.percentile(df_KPI_unique_ids.total_net_deposit_per_bet.values, [1, 5, 10, 25, 50, 75, 90, 95, 99])
    
    ap_scores = [1, 2 , 3, 4, 5]
    naw_scores = [1, 2 , 3, 4, 5]
    bdc_scores = [1, 2 , 3, 4, 5]
    aba_scores = [1, 2 , 3, 4, 5]
    bc_scores = [1, 2 , 3, 4, 5]
    awr_scores = [1, 2 , 3, 4, 5]
    abpv_scores = [1, 2, 3, 4, 5]
    wr_scores =   [5, 4, 3, 2, 1]
    da_scores =  [1, 2, 3, 4, 5, 6]
    wa_scores =  [4, 3, 2, 1]
    nda_scores = [1, 2, 3, 4, 5, 6, 7]
    tdr_scores = [1, 2, 3, 4, 5, 6, 7]
    tnd_scores = [1, 2, 3, 4, 5, 6, 7]
    ndpb_scores = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    
    df_KPI_scores = df_KPI_3periods.copy()   
    df_KPI_scores['ap_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.active_periods, ap_scores, ap_pct), axis = 1)
    df_KPI_scores['naw_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.num_active_weeks, naw_scores, naw_pct), axis = 1)
    df_KPI_scores['awr_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.recent_activity_freq, awr_scores, active_weeks_ratio_pct), axis = 1)
    df_KPI_scores['bdc_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.bet_days_counts, bdc_scores, bet_days_counts_pct), axis = 1)
    df_KPI_scores['aba_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.avg_bet_amount, aba_scores, avg_bet_amount_pct), axis = 1)
    df_KPI_scores['bc_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.bets_counts, bc_scores, bets_counts_pct), axis = 1)
    df_KPI_scores['wr_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.win_ratio, wr_scores, win_ratio_pct), axis = 1)
    df_KPI_scores['abpv_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.avg_bet_per_visit, abpv_scores, avg_bet_per_visit_pct), axis = 1)
    df_KPI_scores['da_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.deposit_amounts, da_scores, deposit_amounts_pct), axis = 1)
    df_KPI_scores['wa_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.withdrawal_amounts, wa_scores, withdrawal_amounts_pct), axis = 1)
    df_KPI_scores['nda_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.net_deposits, nda_scores, net_deposit_amount_pct), axis = 1)
    df_KPI_scores['tdr_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.total_dep_relative, tdr_scores, total_dep_relative_pct), axis = 1)
    df_KPI_scores['tnd_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.total_net_deposit, tnd_scores, total_net_deposit_pct), axis = 1)
    df_KPI_scores['ndpb_scores'] = df_KPI_scores.apply(lambda x: scoring_KPI(x.total_net_deposit_per_bet, ndpb_scores, net_deposit_per_bet_pct), axis = 1)
    
    
    df_KPI_3periods_avg = df_KPI_scores.groupby(level=0).mean()
    df_KPI_3periods_avg['GGR'] = (df_KPI_3periods_avg.bet_amounts  - df_KPI_3periods_avg.payout_amount_w_fb) 
    df_KPI_3periods_avg['GGR_margin'] = (df_KPI_3periods_avg.bet_amounts  - df_KPI_3periods_avg.payout_amount_w_fb) / df_KPI_3periods_avg.bet_amounts
    # there are some customers who don't have betting activity in a given period
    df_KPI_3periods_avg['GGR_margin'].fillna(0, inplace = True)
    df_KPI_3periods_avg['total_GGR'] = (df_KPI_3periods_avg.total_bet_amt  - df_KPI_3periods_avg.total_pay_amt) 
    df_KPI_3periods_avg['total_GGR_margin'] = (df_KPI_3periods_avg.total_bet_amt  - df_KPI_3periods_avg.total_pay_amt) / df_KPI_3periods_avg.total_bet_amt   
    
    
    if f_Output:
        if is_Bally:
            df_KPI_3periods_avg.to_csv(data_path + '%s_bally_for_tiers.csv'%cur_date, index = True)
        else:
            df_KPI_3periods_avg.to_csv(data_path + '%s_elite_for_tiers.csv'%cur_date, index = True)


    
    return df_KPI_3periods_avg
